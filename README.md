
# CRUD-X

A basic CRUD web application using [X]ERN stack.

## Badges

[![MIT License](https://img.shields.io/badge/License-MIT-green.svg)](https://choosealicense.com/licenses/mit/)


## Tech Stack : [X]ERN

**Client:** React (Frontend Library)

**Server:** Express, Node [Future http server - Apache, Ngnix, IIS]

**Multi-DB [X - SQL/NoSQL]:** mongodb, mysql, elasticsearch, cockroachdb, cassandra.

## Features

- Multi-Authentication (Cookies Vs. JWT)
- Multi-Products CRUD
- Authorization & OAuth2.0 Support
- Cross platform & responsive web design


## Installation

Install `CRUD-X` with npm

```bash
  cd CRUD-X
  npm install
```

## Development Env. : IDE | Tools | Library | Framework | Extensions

VCS/SCM : XXXXX (Github | Gitlab) - Pending

Online Web Editor/Sandbox Playground : Pending

Local IDE : VSCode 

Cloud IDE (Remote) : XXXXX (Codespace/vsremote, gitpod, glitch, replit, codesandbox/stackblitz, code-server) - Pending

## Deployment (PaaS | Devops-CI/CD)

PaaS : XXXXX (Vercel, Netlify, heroku etc.) - PENDING

Devops (CI/CD) : XXXXXX (Github Actions, Gitlab CI/CD, Travis CI, Circle CI, Docker, K8 etc.) - PENDING

To deploy this project locally run

**For Backend (localhost:3000/)** : 

```bash
  npm start
```

**For Frontend** build the React Project: 

```bash
  npm run build
```

## Environment Variables

To run this project, you will need to add the following environment variables to your .env file

`API_KEY`

`ANOTHER_API_KEY`


## API Reference

#### To Signup

```http
  GET /register
```

| Parameter | Type     | Description                |
| :-------- | :------- | :------------------------- |
| `None` | `string` | To signup user |

#### To Login

```http
  GET /login
```

| Parameter | Type     | Description                       |
| :-------- | :------- | :-------------------------------- |
| `None`      | `string` | To Login user |




## Demo

![](https://media1.giphy.com/media/wAvzlIA6cRPeDyRjY9/giphy.gif?cid=790b7611de9cb72ce5aa85de257c1cec75ef4ba7982098bf&rid=giphy.gif&ct=g)


## Contributing

Contributions are always Welcome. Make a Pull Request (PR) or raise an issue. Will review them when time permits.


## FAQ

#### Is this CRUD App for commercial use ?

Nope.

#### Is this your Personal project ?

Yup, to clear out my basics of fullstack web app dev. 


## Feedback & Support

If you have any feedback, please reach out to me at sonimonish00[at]gmail[dot]com


## 🚀 About Me : Hi, I'm Monish! 👋
🧠 I'm currently learning backend/full stack development.


## 🔗 Links
[![portfolio](https://img.shields.io/badge/my_portfolio-000?style=for-the-badge&logo=ko-fi&logoColor=white)](https://sonimonish00.github.io/)

[![linkedin](https://img.shields.io/badge/linkedin-0A66C2?style=for-the-badge&logo=linkedin&logoColor=white)](https://www.linkedin.com/in/monishsoni)

[![twitter](https://img.shields.io/badge/twitter-1DA1F2?style=for-the-badge&logo=twitter&logoColor=white)](https://twitter.com/MonishSoni95)


## 🛠 Skills
Python, Javascript (Node, React), HTML, CSS

